<?php

function scrapeDocument($url) {
    // create curl resource
    $ch = curl_init();

    $params = [
        "url" => $url
    ];

    if (isset($_GET['section'])) {
        $params["section"] = $_GET['section'];
    }

    // set url
    curl_setopt($ch, CURLOPT_URL, "http://serverless-ssr.netlify.app/.netlify/functions/scrape_document");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
}

if (isset($_GET['url'])) {
    $url = $_GET['url'];
    $output = scrapeDocument($url);
    echo $output;
}


