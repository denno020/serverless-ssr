# Serverless SSR

This is a proof of concept for serverside rendering using a serverless function and Chromes headless browser, through the Puppeteer package.

## Usage

Send a query to the url `https://serverless-ssr.netlify.app/.netlify/functions/scrape_document`

Include the following parameters as a JSON body:

`url` - The URL which we're going to "SSR"

`section` - [Optional] A DOM selector of the section of the SSR'd page we want to return.
When `section` isn't provided, the entire body html is returned 

## Development

`yarn install`

`yarn start`

Allows for local development of the serverless functions.

Queries should be sent to `http://localhost:9000/.netlify/functions/scrape_document`

`yarn build`

This is really only here for Netlify to call during deployment

`yarn prebuild`

This was added when I was trying to debug the reason for my functions not appearing in the Netlify functions panel.
The issue in the end was I specified the build command as `build`, rather than `yarn build`, so I don't think this prebuild script is required anymore (i.e. it could probably go in the `build` entry)

## Deployment

Deployment is handled automatically when the `master` branch is updated. This is done through Netlify.

## Testing

As this is a POC, I didn't both with testing 😬

## Known Issues

Currently the deployed function doesn't actually work. Everytime the function is executed, the following error is shown

```bash
Error: spawn ETXTBSY
```

I believe this error is coming from `chrome-aws-lambda`, but I haven't been able to find any concrete solutions

Also included in the repo is a `take-screenshot` function. This function comes from  [https://bitsofco.de/how-to-use-puppeteer-in-a-netlify-aws-lambda-function/](https://bitsofco.de/how-to-use-puppeteer-in-a-netlify-aws-lambda-function/). That example seems to work perfectly fine with Netlify, so I decided to pull that code into this repo and deploy to my Netlify instance, to see if I could also get it working. It doesn't work. This must mean that there is an issue with the NodeJS version, as I've updated the dependency versions to match that which is used in the working `take-screenshot` demo (i.e. reverted back to `v2.0.0` for all, rather than the latest, which is >3)
