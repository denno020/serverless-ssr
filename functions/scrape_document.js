const chromium = require('chrome-aws-lambda');

// const isWin = process.platform === "win32";
// const executablePath = isWin ? 'node_modules/puppeteer/.local-chromium/win64-756035/chrome-win/chrome.exe' : chromium.executablePath

/**
 * Request the web page document
 *
 * @param {string} url             The documents URL to load
 * @param {string} sectionSelector A DOM selector query to extract only part of a page
 *
 * @returns {string}
 */
async function getDocument(url, sectionSelector) {
  const browser = await chromium.puppeteer.launch({
    // Required
    executablePath: await chromium.executablePath,

    // Optional
    args: chromium.args,
    defaultViewport: chromium.defaultViewport,
    headless: true
  });

  const page = await browser.newPage();
  await page.goto(url);
  const bodyHTML = await page.evaluate((domQuery) => {
    return new Promise((res) => {
      if (!domQuery) {
        res(document.body.innerHTML);
        return
      }

      const section = document.querySelector(domQuery);

      if (!section) {
        res("");
        return;
      }

      res(section.innerHTML);
    });

  }, sectionSelector);

  await browser.close();

  return bodyHTML;
}

async function handler(event, context, callback) {
  const { url, section } = JSON.parse(event.body);

  try {
    const html = await getDocument(url, section);

    callback(null, {
      statusCode: 200,
      body: html
    });
  } catch (err) {
    callback(new Error(err));
  }
}

exports.handler = handler;
